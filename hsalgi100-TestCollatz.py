#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve, cycle_length

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)
    
    def test_read_2(self):
        s = "100 200\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  100)
        self.assertEqual(j, 200)
    
    def test_read_3(self):
        s = "201 210\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  201)
        self.assertEqual(j, 210)
    
    def test_read_4(self):
        s = "900 1000\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  900)
        self.assertEqual(j, 1000)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)
    
    def test_eval_5(self):
        v = collatz_eval(1, 1000000)
        self.assertEqual(v, 525)
    
    def test_eval_6(self):
        v = collatz_eval(1, 1)
        self.assertEqual(v, 1)
    
    def test_eval_7(self):
        max_cycle_length = collatz_eval(7350, 90845)
        self.assertEqual(max_cycle_length, 351)
    
    def test_eval_7(self):
        max_cycle_length = collatz_eval(77, 777)
        self.assertEqual(max_cycle_length, 171)

    def test_eval_8(self):
        max_cycle_length = collatz_eval(99, 99)
        self.assertEqual(max_cycle_length, 26)

    def test_eval_9(self):
        max_cycle_length = collatz_eval(100000, 555999)
        self.assertEqual(max_cycle_length, 470)
    
    def test_eval_10(self):
        max_cycle_length = collatz_eval(37, 73)
        self.assertEqual(max_cycle_length, 116)


    # ------------
    # cycle length
    # ------------
    def test_cycle_length_1(self):
        number_of_cycles = cycle_length(5)
        self.assertEqual(number_of_cycles, 6)

    def test_cycle_length_2(self):
        number_of_cycles = cycle_length(10)
        self.assertEqual(number_of_cycles, 7)

    def test_cycle_length_3(self):
        number_of_cycles = cycle_length(3)
        self.assertEqual(number_of_cycles, 8)

    def test_cycle_length_4(self):
        number_of_cycles = cycle_length(4)
        self.assertEqual(number_of_cycles, 3)

    def test_cycle_length_5(self):
        number_of_cycles = cycle_length(27)
        self.assertEqual(number_of_cycles, 112)

    def test_cycle_length_6(self):
        number_of_cycles = cycle_length(900)
        self.assertEqual(number_of_cycles, 55)

    def test_cycle_length_7(self):
        number_of_cycles = cycle_length(3283)
        self.assertEqual(number_of_cycles, 75)

    def test_cycle_length_8(self):
        number_of_cycles = cycle_length(1000000)
        self.assertEqual(number_of_cycles, 153)


    
    

    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")
    
    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 100, 200, 125)
        self.assertEqual(w.getvalue(), "100 200 125\n")
    
    def test_print_3(self):
        w = StringIO()
        collatz_print(w, 201, 210, 89)
        self.assertEqual(w.getvalue(), "201 210 89\n")
    
    def test_print_4(self):
        w = StringIO()
        collatz_print(w, 900, 1000, 174)
        self.assertEqual(w.getvalue(), "900 1000 174\n")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
% coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


% cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


% coverage report -m                   >> TestCollatz.out



% cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
